# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.modules.jasper_reports.jasper import JasperReport
from trytond.modules.electrans_reports.jasper import JasperReportWithFileName
from trytond.transaction import Transaction


class PurchaseRequisitionReport(JasperReport):
    __name__ = 'purchase.requisition.electrans'


class PurchaseReport(JasperReportWithFileName(name="number")):
    __name__ = 'purchase.purchase.electrans'

    @classmethod
    def execute(cls, ids, data):
        with Transaction().set_context(address_with_party=True):
            return super(PurchaseReport, cls).execute(ids, data)
