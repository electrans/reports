# -*- encoding: utf-8 -*-
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.modules.electrans_reports.jasper import JasperReportWithFileName
from trytond.modules.company import CompanyReport
from decimal import Decimal
from trytond.transaction import Transaction


class InvoiceReport(JasperReportWithFileName(name="number"), metaclass=PoolMeta):
    __name__ = 'account.invoice'


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    base_summary = fields.Function(
        fields.Numeric('Company Amount',
                       digits=(16, Eval('company_currency_digits', 2)),
                       depends=['company_currency_digits']),
        'get_taxes_resume')
    tax_summary = fields.Function(
        fields.Char('Tax Name'),
        'get_taxes_resume')
    tax_amount_summary = fields.Function(
        fields.Numeric('Tax Amount',
                       digits=(16, Eval('company_currency_digits', 2)),
                       depends=['company_currency_digits']),
        'get_taxes_resume')
    legal_notice_summary = fields.Function(
        fields.Char('Legal Notice'),
        'get_taxes_resume')

    def get_taxes_resume(self, name):
        """
        Return None if the line taxes is 1 or if more than one different tax. In other cases get summary values
        """
        # Check if line taxes is greater than > 1
        if len(self.taxes) > 1:
            # Check if the tax(in all taxes lines) is greater than 1
            line_tax_ids = [line_tax.tax.id for line_tax in self.taxes]
            if len(set(line_tax_ids)) > 1:
                return None
            base_summary = Decimal('0.0')
            tax_summary = self.taxes[0].rec_name
            tax_amount_summary = Decimal('0.0')
            legal_notice_summary = self.taxes[0].legal_notice
            for line in self.taxes:
                base_summary += line.base
                tax_amount_summary += line.amount
        else:
            # line taxes is 1 get tax summary
            return None
        if name == 'base_summary':
            return base_summary
        elif name == 'tax_summary':
            return tax_summary
        elif name == 'tax_amount_summary':
            return tax_amount_summary
        elif name == 'legal_notice_summary':
            return legal_notice_summary


class InvoiceValuedRelationReport(CompanyReport):
    __name__ = 'account.invoice.valued_relation.report'

    @classmethod
    def execute(cls, ids, data):
        pool = Pool()
        SaleLine = pool.get('sale.line')
        with Transaction().set_context(SaleLine=SaleLine):
            return super(InvoiceValuedRelationReport, cls).execute(ids, data)

    @classmethod
    def get_context(cls, records, header, data):
        context = super().get_context(records, header, data)
        invoice = records[0]
        party_lang = invoice.party.lang
        if party_lang:
            context['language'] = party_lang.code
        return context
