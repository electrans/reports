# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import unittest
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.pool import Pool
from trytond.modules.company.tests import create_company, set_company
from trytond.modules.electrans.tests.test_electrans import create_fiscalyear_and_chart, get_accounts
from decimal import Decimal


def create_account_category_goods(tax, expense, revenue):
    pool = Pool()
    ProductCategory = pool.get('product.category')
    account_category_goods = ProductCategory(name="Bienes")
    account_category_goods.accounting = True
    account_category_goods.customer_taxes = [tax]
    account_category_goods.account_expense = expense
    account_category_goods.account_revenue = revenue
    account_category_goods.save()
    return account_category_goods


def creates_product_lot_sequence(account_category_goods, with_sequence=None):
    pool = Pool()
    Template = pool.get('product.template')
    Uom = pool.get('product.uom')
    Product = pool.get('product.product')
    Sequence = pool.get('ir.sequence')
    SequenceType = pool.get('ir.sequence.type')
    unit, = Uom.search([('name', '=', 'Unit')])
    sequence = None
    # Create sequence
    if with_sequence:
        sequence_type, = SequenceType.search([('name', '=', "Stock Lot"), ], limit=1)
        sequence, = Sequence.create([{
            'name': 'Test Lots',
            'sequence_type': sequence_type.id,
            'prefix': 'EM',
            'suffix': '',
            'type': 'incremental',
        }])
    # Create products
    template, = Template.create([{
        'name': 'Product',
        'type': 'goods',
        'purchase_uom': unit.id,
        'purchasable': True,
        'cost_price_method': 'fixed',
        'account_category': account_category_goods.id,
        'default_uom': unit.id,
        'list_price': Decimal(5),
        'producible': True,
        'lot_sequence': sequence if with_sequence else None,
    }])
    product, = Product.create([{
        'template': template.id,
        'cost_price': Decimal(1),
    }])
    return product


class ReportsTestCase(ModuleTestCase):
    'Test Product Purchase Line Relation module'
    module = 'electrans_reports'

    @with_transaction()
    def test_lots_range_delivery_note(self):
        "Test Unit Price Requests"
        pool = Pool()
        ProductStockMove = pool.get('stock.product.move')
        Tax = pool.get('account.tax')
        company = create_company()
        with set_company(company):
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category_goods = create_account_category_goods(tax, expense, revenue)
            # Create product with lot sequence
            product1 = creates_product_lot_sequence(account_category_goods, with_sequence=True)
            self.assertTrue(product1.lot_sequence)
            self.assertEqual(product1.lot_sequence.prefix, 'EM')
            product_stock_move = ProductStockMove()
            product_stock_move.product = product1
            # Test If lot with the same prefix as the prefix of the product sequence (EM)
            # and suffix is not is convertible to int return the suffix literally
            # EXAMPLE --> EM;E1,EM;E2,EM;E3,EM;E4,EM;E5,EM;E6 --> ;E1,;E2,;E3,;E4,;E5,;E6
            # Test If suffix is convertible to int it will search a range around that lot
            # EXAMPLE --> EM123,EM124,EM125,EM126,EM127 --> 123..127
            # Test If the lot has NOT the prefix of the product sequence, lot will be printed literally
            # EXAMPLE --> product.seq.prefix = 'EM' // lot = 'PF;E1', results in --> PF;E1
            product_stock_move.lots = 'EM0001,EM0002,EM0003,EM0005,EM0006,EM0007,EM00015,EM00016,EM00022,EM0001,EM123,EM124,EM125,EM126,EM127,EM;E1,EM;E2,EM;E3,EM;E4,EM;E5,EM;E6,ME;E1,ME;E2,MS;E2,MS;S1/I,MS;S1/II,NP;E1,NP;E2,NP;E3,NP;E4,NP;S2/I,NP;S2/II,PF;E1,PF;E2,PF;E4,PF;S1/I,SE;E1,SE;E2,SE;E3,SE;E4,SE;E6,SE;S1/I,SE;S1/III,TO;E1,TO;S2/I,TO;S2/II,TV;E1,TV;E2,TV;E4,TV;IA1,TV;IA3,VA;E1,VA;E2,VA;E4,VA;E6'
            lots_range = product_stock_move.get_lots_range('lots_range')
            self.assertEqual(lots_range, '0001..0003, 0005..0007, 00015, 00016, 00022, 123..127, ;E1, ;E2, ;E3, ;E4, ;E5, ;E6, ME;E1, ME;E2, MS;E2, MS;S1/I, MS;S1/II, NP;E1, NP;E2, NP;E3, NP;E4, NP;S2/I, NP;S2/II, PF;E1, PF;E2, PF;E4, PF;S1/I, SE;E1, SE;E2, SE;E3, SE;E4, SE;E6, SE;S1/I, SE;S1/III, TO;E1, TO;S2/I, TO;S2/II, TV;E1, TV;E2, TV;E4, TV;IA1, TV;IA3, VA;E1, VA;E2, VA;E4, VA;E6')
            # Test If product does not have lot sequence, the lot will be printed literally
            product2 = creates_product_lot_sequence(account_category_goods)
            self.assertFalse(product2.lot_sequence)
            product_stock_move2 = ProductStockMove()
            product_stock_move2.product = product2
            product_stock_move2.lots = 'TV;E4,TV;IA1,TV;IA3,VA;E1,VA;E2,VA;E4,VA;E6,131,132,133,134,AC/0001'
            lots_range = product_stock_move2.get_lots_range('lots_range')
            self.assertEqual(lots_range, 'TV;E4, TV;IA1, TV;IA3, VA;E1, VA;E2, VA;E4, VA;E6, 131, 132, 133, 134, AC/0001')


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        ReportsTestCase))
    return suite
