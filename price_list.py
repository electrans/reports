# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.modules.jasper_reports.jasper import JasperReport
from trytond.pool import PoolMeta
from trytond.modules.product import price_digits
from trytond.model import fields


class PriceListLine(metaclass=PoolMeta):
    __name__ = 'product.price_list.line'

    computed_formula = fields.Function(
        fields.Numeric('Unit Price', digits=price_digits),
        'get_computed_formula')

    def get_computed_formula(self, name):
        return self.price_list.compute(product=self.product,
            party=None, unit_price=self.product.list_price, quantity=1.0, 
            uom=self.product.default_uom)


class PriceListReport(JasperReport):
    __name__ = 'product.price_list.report'
