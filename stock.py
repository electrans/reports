# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.modules.jasper_reports.jasper import JasperReport
from trytond.modules.company import CompanyReport
from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.modules.electrans_reports.jasper import JasperReportWithFileName
from trytond.pyson import Eval
from trytond.modules.stock.shipment import ShipmentReport
from sql import Column, Literal
from sql.aggregate import Sum, Max, Aggregate
from sql.conditionals import Coalesce
from trytond.wizard import Wizard, StateView, Button
import zipfile


def get_product_lots_range(lots, product_move):
    """
    Get lots by rangs (str)
    Lots: ['L/0001', 'L/0002', 'L/0003', 'L/0005', 'L/0006', 'L/0007', 'L/0015','L/0016', 'L/0022']
    return: '0001 a 0003, 0005 a 0007, 00015, 0016, 0022'
    TO-DO: A range like ['0001A','0002X','0003X'] will result in '0001A..0003X', which is not totally right...
    """
    if lots:
        # create a dictionary with lots without the prefix (if exists) as key and the lots casted to int as a value
        conversion_dict = {}
        # uncommon lots will be the ones that its product
        # does not have lot sequence and prefix defined in it
        uncommon_lots = ''
        uncommon_lots_list = []
        seq = product_move.template.lot_sequence
        for lot in lots.split(','):
            if seq and seq.prefix == lot[:len(seq.prefix)]:
                res = lot[len(seq.prefix):]
                # Check if res can be converted to int
                if res and res.isdigit():
                    conversion_dict.update({res: int(res)})
                else:
                    # Only add suffix of the number
                    uncommon_lots_list.append(res)
            else:
                uncommon_lots_list.append(lot)
        if uncommon_lots_list:
            uncommon_lots = ', '.join(lot for lot in uncommon_lots_list)
        # order the dictionary by the value
        ordered_dict = dict(sorted(conversion_dict.items(), key=lambda item: item[1]))

        # create tuple of consecutive lots sets
        ranges = []
        current_range = []
        previous_lot_num = None
        for lot, lot_num in ordered_dict.items():
            if not previous_lot_num or lot_num != previous_lot_num + 1:
                current_range = []
                ranges.append(current_range)
            current_range.append(lot)
            previous_lot_num = lot_num
        common_lots = ', '.join(
            r[0] + '..' + r[-1] if len(r) > 2 else ', '.join(r) for r in ranges)
        # Control commas so don't show unnecessary punctuation
        all_lots = (common_lots + (', ' if uncommon_lots and common_lots
                                   else '') + uncommon_lots)
        return all_lots

    return ''


class ShipmentOut(metaclass=PoolMeta):
    __name__ = 'stock.shipment.out'

    sales_reference = fields.Function(
        fields.Char("References"),
        'get_sales_field')
    product_moves = fields.One2Many('stock.product.move', 'shipment',
        'Product Moves', readonly=True)
    product_outgoing_moves = fields.Function(fields.One2Many(
            'stock.product.move', 'shipment', 'Outgoing Product Moves'),
        'get_product_outgoing_moves')
    product_inventory_moves = fields.Function(fields.One2Many(
            'stock.product.move', 'shipment', 'Inventory Product Moves'),
        'get_product_inventory_moves')

    @classmethod
    def copy(cls, shipments, default=None):
        if default is None:
            default = {}
        default = default.copy()
        if 'product_moves' not in default:
            default['product_moves'] = None
        return super(ShipmentOut, cls).copy(shipments, default=default)

    def get_sales_field(self, name):
        name = name.split('_')[1]
        result = ''
        for sale in self.sales:
            # check if the result is > 30 because then it do not fits in the report
            if result and len(result + getattr(sale, name)) > 30:
                return result + ' ...'
            if getattr(sale, name):
                result += getattr(sale, name) + ', '
        return result[:-2]

    def get_product_outgoing_moves(self, name):
        moves = []
        moves_without_origin = []
        pool = Pool()
        SaleLine = pool.get('sale.line')
        for move in self.product_moves:
            if move.from_location.id == self.warehouse.output_location.id and \
                    move.quantity != 0:
                if isinstance(move.origin, SaleLine) and Transaction().context.get('order'):
                    if move.origin.parent:
                        moves.append((move, move.origin.parent.position))
                    else:
                        moves.append((move, move.origin.position))
                else:
                    moves_without_origin.append(move)
        if Transaction().context.get('order'):
            ordered_moves = []
            for move in sorted(moves, key=lambda move: (move[1], move[0].origin.id)):
                ordered_moves.append(move[0])
            ordered_moves += moves_without_origin
        else:
            ordered_moves = moves_without_origin
        return ordered_moves

    def get_product_inventory_moves(self, name):
        moves = []
        Move = Pool().get('stock.move')
        for move in self.product_moves:
            if move.to_location.id == self.warehouse.output_location.id:
                moves.append(move.id)
        context = Transaction().context
        if context.get('ordered_location_moves'):
            moves = Move.search([('id', 'in', moves)],
                                order=[('from_location', 'ASC')])
        return moves

    @property
    def package_dict_list(self):
        pool = Pool()
        Uom = pool.get('product.uom')
        ModelData = pool.get('ir.model.data')
        cubic_meter = Uom(ModelData.get_id('product', 'uom_cubic_meter'))
        code_type_list = []
        packages_dict = {}
        total_summary = {}
        total_net_weight = 0
        total_gross_weight = 0
        total_volume = 0
        packages_with_moves = list(filter(lambda x: len(x.moves) > 0, self.packages))
        for package in packages_with_moves:
            product_dict = {}
            total_package_volume = 0
            # Compute total package volume in cubic meters
            if package.package_volume and package.package_volume_uom == cubic_meter:
                total_package_volume = package.package_volume
            elif package.package_volume:
                total_package_volume = Uom.compute_qty(
                    package.package_volume_uom, package.package_volume, cubic_meter, round=False)
            for i, move in enumerate(package.moves):
                product_code = move.product.code if move.product else ''
                key = (
                    move.product, move.production_input, move.production_output, move.from_location, move.to_location,
                    move.state, move.description, move.origin)
                if key not in product_dict:
                    # Initialize package cause package data only is shown in first line
                    product_dict[key] = {
                        'package_code': package.code if package.code and i == 0 else '',
                        'package_type': package.type.name if package.type and i == 0 else '',
                        'package_total_weight': package.total_weight if i == 0 else 0,
                        'package_height': package.height if package.height and i == 0 else 0,
                        'height_uom': package.height_uom.symbol if package.height_uom and i == 0 else '',
                        'package_length': package.length if package.length and i == 0 else 0,
                        'length_uom': package.length_uom.symbol if package.length_uom and i == 0 else '',
                        'package_width': package.width if package.width and i == 0 else 0,
                        'width_uom': package.width_uom.symbol if package.width_uom and i == 0 else '',
                        'package_volume': total_package_volume if total_package_volume and i == 0 else 0,
                        'product_move': move.product,
                        'product_code': product_code,
                        'quantity': 0,
                        'uom': move.product.default_uom.symbol if move.product.default_uom else '',
                        'description': move.description,
                        'serial_numbers': [],
                        'unit_weight': move.product.weight if move.product.weight else 0,
                        'unit_weight_uom': move.product.weight_uom.symbol if move.product.weight_uom else '',
                        'net_weight': 0,
                    }
                # Concat lot of product move grouped by key
                product_dict[key]['serial_numbers'].append(
                    move.lot.number if move.lot and move.product and move.product.serial_number else '')
                # Get quantity already converted to the default uom of the product
                product_dict[key]['quantity'] += move.internal_quantity
                # Internal weight is already converted to kg so his total too
                product_dict[key]['net_weight'] += move.internal_weight if move.internal_weight else 0
                # Sum net weight
                # In Kg
                total_net_weight += move.internal_weight if move.internal_weight else 0

            # Get product lots range
            for key, value in product_dict.items():
                product_serial_numbers = ','.join(lot for lot in value['serial_numbers'] if lot)
                if product_serial_numbers:
                    product_serial_numbers = get_product_lots_range(product_serial_numbers, value['product_move'])
                product_dict[key]['serial_numbers'] = product_serial_numbers
            # Sum gross_weight and volume
            total_gross_weight += package.total_weight
            # In liters
            total_volume += total_package_volume
            code_type_list.extend(product_dict.values())
        # Save totals values in a dict
        total_summary['total_net_weight'] = total_net_weight
        total_summary['total_gross_weight'] = total_gross_weight
        total_summary['total_volume'] = total_volume
        # Only count packages that have moves
        total_summary['total_packages'] = len(packages_with_moves)
        # Save totals and packages list separated in a dict
        packages_dict['code_type_list'] = code_type_list
        packages_dict['totals_summary'] = total_summary
        return packages_dict


class DeliveryNote(JasperReportWithFileName(name="number")):
    __name__ = 'stock.shipment.out.delivery_note.electrans'

    @classmethod
    def render(cls, report, data, model, ids):
        with Transaction().set_user(0):
            with Transaction().set_context(order=True):
                return super(DeliveryNote, cls).render(report, data, model, ids)


class ShipmentInternalOutReport(JasperReportWithFileName(name="number")):
    __name__ = 'stock.shipment.internal.out.electrans'

    @classmethod
    def render(cls, report, data, model, ids):
        data['parameters'] = {'print_serial_numbers_and_lots': '2'}
        with Transaction().set_user(0):
            res = super(ShipmentInternalOutReport, cls).render(report, data, model, ids)
        return res


class ShipmentInReturnReport(JasperReportWithFileName(name="number")):
    __name__ = 'stock.shipment.in.return.electrans'

    @classmethod
    def render(cls, report, data, model, ids):
        with Transaction().set_user(0):
            res = super(ShipmentInReturnReport, cls).render(report, data, model, ids)
        return res


class PickingList(ShipmentReport):
    'Picking List'
    __name__ = 'stock.shipment.out.picking_list.electrans'

    @classmethod
    def get_context(cls, shipments, header, data):
        """
        Override the context to add the order to the context moves
        """
        report_context = super().get_context(shipments, header, data)
        # sorted_moves[shipment.id] = sorted(sorted_moves[shipment.id])
        for shipment in shipments:
            report_context['moves'][shipment.id] = sorted(
                report_context['moves'][shipment.id],
                key=lambda move: (move.from_location.name, move.product.code))
        return report_context

    @classmethod
    def moves(cls, shipment):
        if shipment.warehouse_storage == shipment.warehouse_output:
            with Transaction().set_context(order=True):
                return shipment.product_outgoing_moves
        else:
            with Transaction().set_context(ordered_location_moves=True):
                return shipment.product_inventory_moves


class PickingListOrderedByLocation(JasperReport):
    __name__ = 'stock.shipment.out.picking_list.order.location.electrans'

    @classmethod
    def render(cls, report, data, model, ids):
        with Transaction().set_user(0):
            Transaction().set_context(ordered_location_moves=True)
            res = super(PickingListOrderedByLocation, cls).render(report, data, model,
                                                                  ids)
        return res


class StringAgg(Aggregate):
    __slots__ = ('separator', 'order')
    _sql = 'STRING_AGG'

    def __init__(self, expression, separator, order=None):
        super(StringAgg, self).__init__(expression)
        if order is None:
            order = 'ASC'
        self.separator = Literal(separator)
        self.order = order

    def __str__(self):
        return '%s(%s, %s ORDER BY %s %s)' % (self._sql, self.expression,
            self.separator, self.expression, self.order)

    @property
    def params(self):
        return (self.expression.params + self.separator.params
            + self.expression.params)


class ConformityReports(Wizard):
    'Conformity Report wizard'
    __name__ = 'stock.shipment.out.conformity'

    start = StateView('stock.shipment.out.conformity.end',
        'electrans_reports.shipment_out_conformity_end_view_form', [
            Button('Close', 'end', 'tryton-cancel'),
            ])

    def default_start(self, fields):
        pool = Pool()
        ActionReport = pool.get('ir.action.report')
        Shipment = pool.get('stock.shipment.out')

        ids = Transaction().context['active_ids']
        action_report, = ActionReport.search([
            ('report_name', '=', 'stock.shipment.out.conformity.electrans')
            ])

        v_list = []
        for shipment in Shipment.browse(ids):
            id_ = shipment.id
            alternative_report = False
            reports = {}
            # TODO 352 -> ID of report
            data = {
                'model': 'stock.shipment.out',
                'action_id': 352,
                'ids': ids,
                'id': id_,
                }

            alternative_reports = shipment.customer.alternative_reports

            if alternative_reports:
                report = None
                for alt_report in alternative_reports:
                    if alt_report.model_name == 'stock.shipment.out':
                        report = alt_report
                        alternative_report = True
                        data['model'] = 'stock.product.move'
                if report:
                    reports[report.report] = [id_]
                else:
                    reports[action_report] = [id_]
            else:
                reports[action_report] = [id_]

            if alternative_report:
                filename = shipment.filename

                report = list(reports.keys())[0]
                reports = self.render_products(report, data, filename)

                zf = zipfile.ZipFile(filename, 'w')
                for rep, pdf_name in reports:
                    type, data, pages = rep
                    zf.writestr(pdf_name, data)
                zf.close()
                f = open(filename, 'r')
                conf_file = self.create_files(
                        f.read(), shipment, filename
                        )
                f.close()

            else:
                filename = 'declaracion-de-conformidad.pdf'
                report = list(reports.keys())[0]
                model = data.get('model')
                ids = data.get('ids')
                _, data, pages = ConformityReport().render(
                        report, data, model, ids
                        )
                conf_file = self.create_files(data, shipment, filename)

            v_list.append(conf_file)

        return {
            'files': v_list,
            }

    def create_files(self, file, shipment, filename):
        conformity_file = {
            'conformity_file': file,
            'filename': filename,
            'shipment': shipment.id,
        }
        return conformity_file

    def render_products(self, report, data, filename):
        pool = Pool()
        Shipment = pool.get('stock.shipment.out')
        shipment = Shipment(data['id'])
        reports = []
        for product_move in shipment.product_inventory_moves:
            data = {
                'model': 'stock.product.move',
                'action_id': data['action_id'],
                'ids': [product_move.id],
                'id': product_move.id,
            }
            model = data.get('model')
            ids = data.get('ids')
            pdf_name = filename[:-4] + '.pdf'
            reports.append([
                ConformityReport().render(report, data, model, ids), pdf_name
                ])
        return reports


class ConformityReportEnd(ModelView):
    'Conformity report wizard end'
    __name__ = 'stock.shipment.out.conformity.end'

    files = fields.One2Many('stock.shipment.out.conformity.binaries',
        'files', 'Files')


class ConformityBinaries(ModelView, metaclass=PoolMeta):
    'Conformity Report Binaries'
    __name__ = 'stock.shipment.out.conformity.binaries'

    files = fields.Many2One('stock.shipment.out.conformity.end',
        'Files', ondelete='CASCADE')
    conformity_file = fields.Binary('Conformity file', filename='filename',
         readonly=True)
    filename = fields.Char('Filename', readonly=True)
    shipment = fields.Many2One('stock.shipment.out', 'Shipment', readonly=True)


class ConformityReport(JasperReport):
    __name__ = 'stock.shipment.out.conformity.electrans'

    @classmethod
    def multirender(cls, reports, data):
        allpages = 0
        shipment_reports_cache = []
        for report, ids in list(reports.items()):
            model = data.get('model')
            cls.update_data(report, data)
            type, data_file, pages = cls.render(report, data, model, ids)
            shipment_reports_cache.append(data_file)

        if len(shipment_reports_cache) > 1:
            alldata = JasperReport.merge_pdfs(shipment_reports_cache)
        else:
            alldata = shipment_reports_cache[0]
        return (type, alldata, allpages)

    @classmethod
    def render(cls, report, data, model, ids):
        return super(ConformityReport, cls).render(report, data, model, ids)

    @classmethod
    def update_data(cls, report, data):
        pass


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'


class ProductStockMove(ModelSQL, ModelView, Move):
    'Stock Move By Product'
    __name__ = 'stock.product.move'
    product = fields.Many2One('product.product', 'Product', required=True,
        domain=[
            ('type', '!=', 'service'),
            ])
    uom = fields.Function(fields.Many2One('product.uom', 'Unit'),
        'on_change_with_uom')
    uom_digits = fields.Function(fields.Integer('Unit Digits'),
        'on_change_with_uom_digits')
    description = fields.Text('Description')
    quantity = fields.Float('Quantity', required=True,
        digits=(16, Eval('uom_digits', 2)),
        depends=['uom_digits'])
    from_location = fields.Many2One('stock.location', 'From Location',
        domain=[
            ('type', 'not in', ('warehouse', 'view')),
            ])
    to_location = fields.Many2One('stock.location', 'To Location',
        domain=[
            ('type', 'not in', ('warehouse', 'view')),
            ])
    shipment = fields.Reference('Shipment', selection='get_shipment',
        readonly=True, select=True)
    lots = fields.Char('Lots')
    lots_range = fields.Function(fields.Char('Lots Rang'),
        'get_lots_range')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('assigned', 'Assigned'),
        ('done', 'Done'),
        ('cancel', 'Canceled'),
        ], 'State', select=True, readonly=True)
    company = fields.Many2One('company.company', 'Company')
    production_input = fields.Many2One('production', 'Production Input',
        domain=[
            ('company', '=', Eval('company')),
            ],
        depends=['company'])
    production_output = fields.Many2One('production', 'Production Output',
        domain=[
            ('company', '=', Eval('company')),
            ],
        depends=['company'])
    origin = fields.Reference('Origin', selection=[
                (None, ''),
                ('sale.line', 'Sale Line'),
                ('repair.repair', 'Repair'),
            ])
    component_breakdown = fields.Text('Component Breakdown')

    @classmethod
    def table_query(cls):
        pool = Pool()
        Move = pool.get('stock.move')
        Lot = pool.get('stock.lot')
        table = Move.__table__()
        lot = Lot.__table__()
        group_by = []
        for name in ['company', 'product', 'shipment', 'production_input',
                'production_output', 'from_location', 'to_location', 'state',
                'description', 'origin', 'component_breakdown']:
            group_by.append(Column(table, name))
        columns = [] + group_by
        columns.append(Max(table.id).as_('id'))
        columns.append(Max(table.create_date).as_('create_date'))
        columns.append(Max(table.create_uid).as_('create_uid'))
        columns.append(Max(table.write_uid).as_('write_uid'))
        columns.append(Max(table.write_date).as_('write_date'))
        columns.append(Sum(table.internal_quantity).as_('quantity'))
        columns.append(StringAgg(Coalesce(lot.number, ''), ',').as_('lots'))
        return table.join(lot, type_='LEFT',
            condition=(lot.id == table.lot)).select(*columns,
                group_by=group_by)

    @fields.depends('product')
    def on_change_with_uom(self, name=None):
        if self.product:
            return self.product.default_uom.id

    @fields.depends('product')
    def on_change_with_uom_digits(self, name=None):
        if self.product:
            return self.product.default_uom.digits
        return 2

    @staticmethod
    def _get_shipment():
        'Return list of Model names for shipment Reference'
        return [
            'stock.shipment.in',
            'stock.shipment.out',
            'stock.shipment.out.return',
            'stock.shipment.in.return',
            'stock.shipment.internal',
            ]

    @classmethod
    def get_shipment(cls):
        IrModel = Pool().get('ir.model')
        models = cls._get_shipment()
        models = IrModel.search([
                ('model', 'in', models),
                ])
        return [(None, '')] + [(m.model, m.name) for m in models]

    def get_lots_range(self, name):
        lots_range = get_product_lots_range(self.lots, self.product)
        return lots_range


class ShipmentInReturn(metaclass=PoolMeta):
    __name__ = 'stock.shipment.in.return'

    product_moves = fields.One2Many('stock.product.move', 'shipment',
        'Product Moves', readonly=True)
    product_outgoing_moves = fields.Function(fields.One2Many(
            'stock.product.move', 'shipment', 'Outgoing Product Moves'),
        'get_product_outgoing_moves')

    @classmethod
    def copy(cls, shipments, default=None):
        if default is None:
            default = {}
        default = default.copy()
        if not 'product_moves' in default:
            default['product_moves'] = None
        return super(ShipmentInReturn, cls).copy(shipments, default=default)

    def get_product_outgoing_moves(self, name):
        return [m.id for m in self.product_moves]


class ShipmentDrop(metaclass=PoolMeta):
    __name__ = 'stock.shipment.drop'

    product_moves = fields.One2Many('stock.product.move', 'shipment',
        'Product Moves', readonly=True)
    product_outgoing_moves = fields.Function(fields.One2Many(
            'stock.product.move', 'shipment', 'Outgoing Product Moves'),
        'get_product_outgoing_moves')

    def get_product_outgoing_moves(self, name):
        return [m.id for m in self.product_moves if m.from_location.type == 'drop']


class ShipmentInternal(metaclass=PoolMeta):
    __name__ = 'stock.shipment.internal'

    product_moves = fields.One2Many('stock.product.move', 'shipment',
        'Product Moves', readonly=True)
    product_outgoing_moves = fields.Function(fields.One2Many(
            'stock.product.move', 'shipment', 'Outgoing Product Moves'),
        'get_product_outgoing_moves')

    @classmethod
    def copy(cls, shipments, default=None):
        if default is None:
            default = {}
        default = default.copy()
        if not 'product_moves' in default:
            default['product_moves'] = None
        return super(ShipmentInternal, cls).copy(shipments, default=default)

    def get_product_outgoing_moves(self, name):
        if not self.transit_location:
            return [m.id for m in self.product_moves]
        return [m.id for m in self.product_moves if m.to_location == self.transit_location]


class PackageListReport(CompanyReport):
    __name__ = 'stock.package_list.report'

    @classmethod
    def execute(cls, ids, data):
        with Transaction().set_context(address_with_party=True):
            return super(PackageListReport, cls).execute(ids, data)

    @classmethod
    def get_context(cls, records, header, data):
        pool = Pool()
        Date = pool.get('ir.date')
        context = super().get_context(records, header, data)
        context['today'] = Date.today()
        return context
