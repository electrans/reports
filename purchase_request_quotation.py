# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction


class PurchaseRequestQuotationReport(metaclass=PoolMeta):
    __name__ = 'purchase.request.quotation'

    @classmethod
    def get_context(cls, records, header, data):
        pool = Pool()
        Employee = pool.get('company.employee')

        context = super().get_context(records, header, data)
        employee_id = Transaction().context.get('employee')
        employee = Employee(employee_id) if employee_id else None
        context['employee'] = employee

        return context
