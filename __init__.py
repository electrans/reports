# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import stock
from . import sale
from . import purchase
from . import invoice
from . import price_list
from . import purchase_request_quotation


def register():
    Pool.register(
        price_list.PriceListLine,
        stock.ConformityBinaries,
        stock.ConformityReportEnd,
        stock.ShipmentDrop,
        stock.ProductStockMove,
        stock.ShipmentInternal,
        stock.ShipmentInReturn,
        stock.ShipmentOut,
        invoice.Invoice,
        module='electrans_reports', type_='model')
    Pool.register(
        sale.SaleReport,
        sale.SaleLeadTimes,
        stock.ConformityReport,
        stock.DeliveryNote,
        stock.PackageListReport,
        stock.PickingList,
        stock.PickingListOrderedByLocation,
        stock.ShipmentInternalOutReport,
        stock.ShipmentInReturnReport,
        purchase.PurchaseRequisitionReport,
        purchase.PurchaseReport,
        price_list.PriceListReport,
        invoice.InvoiceReport,
        invoice.InvoiceValuedRelationReport,
        purchase_request_quotation.PurchaseRequestQuotationReport,
        module='electrans_reports', type_='report')
    Pool.register(
        stock.ConformityReports,
        module='electrans_reports', type_='wizard')
