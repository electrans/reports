# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.modules.jasper_reports.jasper import JasperReport
from trytond.pool import Pool


def JasperReportWithFileName(name=False):

    class JasperReportWithFileName(JasperReport):
        report_name = "number"

        @classmethod
        def execute(cls, ids, data):
            model = data.get('model')
            result = super(JasperReportWithFileName, cls).execute(ids, data)
            if len(ids) == 1 and model:
                record = Pool().get(model)(ids[0])
                number = getattr(record, cls.report_name, None)
                if number:
                    result = result[:3] + (number.replace('/', '-'),)
            return result

    JasperReportWithFileName.report_name = name
    return JasperReportWithFileName
