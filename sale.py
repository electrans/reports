# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.modules.electrans_reports.jasper import JasperReportWithFileName
from trytond.report import Report
from trytond.pool import Pool
from trytond.transaction import Transaction


class SaleReport(JasperReportWithFileName(name="full_number")):
    __name__ = 'sale.electrans'


class SaleLeadTimes(Report):
    __name__ = 'sale.lead.times'

    @classmethod
    def render(cls, *args, **kwargs):
        """
        Override to Set lanuage report from sale.lang field
        """
        lang = None
        Sale = Pool().get('sale.sale')
        actions = iter(args)
        for report, values in zip(actions, actions):
            lang = Sale(values['record'].id).lang
        with Transaction().set_context(language=lang.code):
            return super().render(*args, **kwargs)
